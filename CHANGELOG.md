# Changelog
All notable changes to this repository will be documented in this file.

## [0.1] - 2019-08-06
### Added
- initial version of Documentation folder structure.
- _.gitignore_ file with the exceptions required by this repository.
  
## [0.2] - 2024-11-06
### Added
- Useful version of some Microsoft Office templates such as PowerPoint styles and example of Letter in Word.
- _.gitignore_ file with adjust of some folders used by the Finder (MacOS) file explorer.

### Removed
- Delete unused folders and deprecated templates.
