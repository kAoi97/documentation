<img width="200" src="https://gitlab.com/kAoi97/brand/raw/master/Imagotype/bitmap/horisontal/kAoi97%20horisontal%20imagotype.png"><hr />

# Documentation #

Repository with public documentation and formats available for use in areas related to kAoi97 activities


## Office Automation Templates
<img width="400" src="https://gitlab.com/kAoi97/documentation/-/raw/master/Resources/Others/templates_repo.png" >

Bearing in mind that remote work is a constant in the projects and activities carried out under the kAoi97 brand, the different formats provided through this repository are intended to streamline joint work and bring good practices within reach when it comes to meeting the requirements set out in the brand manual, without requiring extra work to adjust and apply the brand to the content produced.

Each folder contains a common group of formats frequently used on different platforms and programs available for each of these, such as some spreadsheets, presentation styles or letterheads for different types of documents.

Please note that the templates and documents included here are for the exclusive use of the kAoi97 brand. Any type of amendment, impersonation or fraud resulting from the use of the content of this repository will be reported to the competent authorities.

## Use 

If you have cloned this repository before or have a downloaded copy, do not forget to check the [changelog](CHANGELOG.md) to make sure you 
are working with the _latest changes uploaded_ to the repository

## Support

If you have any doubts or suggestions when using the elements provided in this repository, do not hesitate to contact me 
through the different means and networks available below, I will be attentive to answer any questions in the shortest 
possible time

[www.kaoi97.net](https://www.kaoi97.net)  

[leonardo@kaoi97.net](mailto:leonardo@kaoi97.net)  

[(+57)314 4818811](https://t.me/kAoi97)  


## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">kAoi97 Brand</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/LeoElBaku" property="cc:attributionName" rel="cc:attributionURL">Leonardo Alvarado Guio</a>
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />
Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.kaoi97.net/terms-and-conditions" rel="cc:morePermissions">https://www.kaoi97.net/terms-and-conditions</a>.